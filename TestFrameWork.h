//
//  TestFrameWork.h
//  TestFrameWork
//
//  Created by Ratnesh Shukla on 30/06/15.
//  Copyright (c) 2015 Ratnesh Shukla. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "ACL.h"
#import "Analytics.h"
#import "AnonymousUtils.h"
#import "Cloud.h"
#import "Config.h"
#import "Constant.h"
#import "File.h"
#import "GeoPoint.h"
#import "Object+Subclass.h"
#import "Object.h"
#import "Query.h"
#import "Relation.h"
#import "Role.h"
#import "Session.h"
#import "Subclassing.h"
#import "User.h"
#import "Installation.h"
#import "NetworkActivityIndicatorManager.h"
#import "Nullability.h"
#import "Product.h"
#import "Purchase.h"
#import "Push.h"
#import "TwitterUtils.h"


@interface TestFrameWork : NSObject

///--------------------------------------
/// @name Connecting to TestServer
///--------------------------------------

/*!
 @abstract Sets the applicationId and clientKey of your application.
 
 @param applicationId The application id of your Parse application.
 @param clientKey The client key of your Parse application.
 */
+ (void)setApplicationId:(NSString *)applicationId clientKey:(NSString *)clientKey;

/*!
 @abstract The current application id that was used to configure Parse framework.
 */
+ (NSString *)getApplicationId;

/*!
 @abstract The current client key that was used to configure Parse framework.
 */
+ (NSString *)getClientKey;

///--------------------------------------
/// @name Enabling Local Datastore
///--------------------------------------

/*!
 @abstract Enable pinning in your application. This must be called before your application can use
 pinning. The recommended way is to call this method before `setApplicationId:clientKey:`.
 */
+ (void)enableLocalDatastore;

/*!
 @abstract Flag that indicates whether Local Datastore is enabled.
 
 @returns `YES` if Local Datastore is enabled, otherwise `NO`.
 */
+ (BOOL)isLocalDatastoreEnabled;

///--------------------------------------
/// @name Enabling Extensions Data Sharing
///--------------------------------------
+ (void)enableDataSharingWithApplicationGroupIdentifier:(NSString *)groupIdentifier
                                  containingApplication:(NSString *)bundleIdentifier;

/*!
 @abstract Application Group Identifier for Data Sharing
 
 @returns `NSString` value if data sharing is enabled, otherwise `nil`.
 */
+ (NSString *)applicationGroupIdentifierForDataSharing;

/*!
 @abstract Containing application bundle identifier.
 
 @returns `NSString` value if data sharing is enabled, otherwise `nil`.
 */
+ (NSString *)containingApplicationBundleIdentifierForDataSharing;


///--------------------------------------
/// @name Logging
///--------------------------------------

/*!
 @abstract Sets the level of logging to display.
 
 @discussion By default:
 - If running inside an app that was downloaded from iOS App Store - it is set to <PFLogLevelNone>
 - All other cases - it is set to <PFLogLevelWarning>
 
 @param logLevel Log level to set.
 @see PFLogLevel
 */
+ (void)setLogLevel:(PFLogLevel)logLevel;

/*!
 @abstract Log level that will be displayed.
 
 @discussion By default:
 - If running inside an app that was downloaded from iOS App Store - it is set to <PFLogLevelNone>
 - All other cases - it is set to <PFLogLevelWarning>
 
 @returns A <PFLogLevel> value.
 @see PFLogLevel
 */
+ (PFLogLevel)logLevel;



@end
